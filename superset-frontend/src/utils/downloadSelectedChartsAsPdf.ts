/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import { t } from '@superset-ui/core';
import kebabCase from 'lodash/kebabCase';
import { SyntheticEvent } from 'react';
import { jsPDF } from 'jspdf';
// @ts-ignore
import html2canvas from 'html2canvas/dist/html2canvas.esm';
import _ from 'lodash';
import { addWarningToast } from 'src/components/MessageToasts/actions';

/**
 * generate a consistent file stem from a description and date
 *
 * @param description title or description of content of file
 * @param date date when file was generated
 */
const generateFileStem = (description: string, date = new Date()) =>
  `${kebabCase(description)}-${date.toISOString().replace(/[: ]/g, '-')}`;

const getScreenshotNodeSelector = (chartId: string | number) =>
  `.dashboard-chart-id-${chartId}`;

/**
 * Create an event handler for turning an element into an image
 *
 * @param selector css selector of the parent element which should be turned into image
 * @param description name or a short description of what is being printed.
 *   Value will be normalized, and a date as well as a file extension will be added.
 * @param isExactSelector if false, searches for the closest ancestor that matches selector.
 * @returns event handler
 */
export default function downloadSelectedChartsAsPdf(
  ids: number[],
  description: string,
  isExactSelector = false,
) {
  return (event: SyntheticEvent) => {
    if (ids.length === 0) {
      return new Promise((resolve, reject) => {
        resolve(addWarningToast(t('Please check the chart first.')));
      });
    }

    const pdfDom = document.createElement('div');
    pdfDom.setAttribute('class', 'pdf-flag-doms');
    document.body.appendChild(pdfDom);

    const idLength = ids.length;
    const yesDoms: (HTMLElement | null)[] = ids.map(id =>
      document.querySelector(getScreenshotNodeSelector(id)),
    );

    const pdf = new jsPDF({
      orientation: 'p',
      unit: 'px',
      format: 'a4',
      hotfixes: ['px_scaling'],
    }); // a4
    const PAGE_W = 793;
    const PAGE_H = 1122;
    const PAGE_MARGIN = 38;

    const genCanvas = (flagDom: HTMLElement | null, lastPosition: number) =>
      new Promise((resolve, reject) => {
        if (!flagDom) {
          reject(new Error('flagDOm is null'));
          return;
        }
        let pdfPosition = lastPosition;
        const checkbox = flagDom.querySelector('[name=chart-check-box]');

        const antdCheckbox = _.get(checkbox, [
          'parentElement',
          'parentElement',
        ]);

        if (checkbox) {
          antdCheckbox!.style.display = 'none';
        }

        html2canvas(flagDom, {
          allowTaint: true,
          width: flagDom.offsetWidth,
          height: flagDom.offsetHeight,
          scale: 1,
        }).then((canvas: HTMLCanvasElement) => {
          try {
            const contentWidth = canvas.width;
            const contentHeight = canvas.height;
            const pageData = canvas.toDataURL('image/jpeg', 1.0);
            // console.log('canvas: width', contentWidth);
            // console.log('canvas: height', contentHeight);

            const diffWidth = contentWidth / (PAGE_W - PAGE_MARGIN * 2);
            const diffHeight = contentHeight / (PAGE_H - PAGE_MARGIN * 2);
            const ratio =
              Math.max(diffWidth, diffHeight) < 1
                ? 1
                : 1 / Math.max(diffWidth, diffHeight);
            const imgWidth = ratio * contentWidth;
            const imgHeight = ratio * contentHeight;

            if (pdfPosition + PAGE_MARGIN + imgHeight > PAGE_H - PAGE_MARGIN) {
              pdf.addPage();
              pdfPosition = 0;
            }

            pdf.addImage(
              pageData,
              'jpeg',
              PAGE_MARGIN,
              pdfPosition + PAGE_MARGIN,
              imgWidth,
              imgHeight,
            );
            pdfPosition += imgHeight + 5;
            resolve(pdfPosition);
          } catch (error) {
            reject(error);
          } finally {
            antdCheckbox.style.display = 'inline';
          }
        });
      });

    // eslint-disable-next-line consistent-return
    return new Promise((resolve, reject) => {
      let index = 0;
      function next(position: number) {
        if (index >= idLength) {
          resolve(position);
          return;
        }

        const chartDOM = yesDoms[index];
        const bigDiv = chartDOM.querySelector(
          "div[data-test-viz-type='table']",
        )?.parentElement;

        let containerDiv1 = null;
        let containerDiv2 = null;
        let containerDiv3 = null;
        let containerDiv4 = null;
        let containerDiv5 = null;
        let bigDivHeight: number;

        let containerDiv1Height = 0;
        let containerDiv5Height = 0;
        let containerDiv1Width = 0;
        let containerDiv5Width = 0;

        if (bigDiv) {
          containerDiv1 = chartDOM.querySelector(
            "div[data-test-viz-type='table'] > .dashboard-chart",
          ) as HTMLParagraphElement;
          containerDiv2 = containerDiv1?.getElementsByTagName('div')[1];
          containerDiv3 = containerDiv1?.getElementsByTagName('div')[4];
          containerDiv4 = containerDiv1?.getElementsByTagName('div')[5];
          containerDiv5 = containerDiv4?.children[1];
          if (bigDiv?.parentElement) {
            bigDivHeight = bigDiv.parentElement.offsetHeight;
          }

          containerDiv1Height = containerDiv1?.offsetHeight;
          containerDiv1Width = containerDiv1?.offsetWidth;
          containerDiv5Height = containerDiv5?.offsetHeight;
          containerDiv5Width = containerDiv5?.offsetWidth;
        }

        let tableDiv = null;
        let tableDiv1 = null;
        let tableDiv2 = null;
        let tableDiv3 = null;
        let tableDiv4 = null;
        let tableDiv5 = null;

        let tableDivHeight = 0;
        let tableDivWidth = 0;
        let tableDiv3Height = 0;
        let tableDiv5Height = 0;

        if (
          yesDoms[index]?.children[1].getAttribute('data-test-viz-type') ===
          'table'
        ) {
          if (bigDiv?.parentElement) {
            bigDiv.parentElement.setAttribute('style', 'height: auto');
          }

          containerDiv2.style.height = 'auto';
          containerDiv2.style.width = 'auto';

          containerDiv3.style.height = 'auto';
          containerDiv3.style.width = 'auto';

          containerDiv4.style.height = 'auto';
          containerDiv4.style.width = 'auto';

          containerDiv5.style.height = 'auto';
          containerDiv5.style.width = 'auto';
        }

        if (
          yesDoms[index]?.children[1].getAttribute('data-test-viz-type') ===
          'pivot_table'
        ) {
          // 获取第二个需要的dom元素
          tableDiv = document.querySelector(
            "div[data-test-viz-type='pivot_table']",
          )?.parentElement?.parentElement as HTMLParagraphElement;
          tableDiv1 = document.querySelector(
            "div[data-test-viz-type='pivot_table'] > .dashboard-chart",
          ) as HTMLParagraphElement;
          tableDiv2 = document.querySelector(
            "div[data-test-viz-type='pivot_table'] .pivot_table",
          )?.children[0] as HTMLParagraphElement;
          tableDiv3 = document.querySelector(
            "div[data-test-viz-type='pivot_table'] .dataTables_scrollBody",
          ) as HTMLParagraphElement;
          tableDiv4 = document.querySelector(
            "div[data-test-viz-type='pivot_table'] .dataTables_scrollHead",
          ) as HTMLParagraphElement;
          tableDiv5 = document.querySelector(
            "div[data-test-viz-type='pivot_table'] .dataTables_scrollHeadInner",
          ) as HTMLParagraphElement;

          tableDivHeight = tableDiv.offsetHeight;
          tableDivWidth = tableDiv.offsetWidth;
          tableDiv3Height = tableDiv3.offsetHeight;
          tableDiv5Height = tableDiv5.offsetWidth + 10;

          tableDiv.style.width = `${tableDiv5Height}px`;
          tableDiv.style.maxWidth = `${tableDiv5Height}px`;
          tableDiv.style.height = 'auto';
          tableDiv.style.maxHeight = 'auto';
          tableDiv1.style.overflow = 'unset';
          tableDiv2.style.overflow = 'unset';
          tableDiv3.setAttribute('style', 'max-height: auto; overflow: unset');
          tableDiv4.style.overflow = 'unset';
        }

        genCanvas(yesDoms[index], position)
          .then((lastPosition: number) => {
            // 重置表单回到原来的状态
            if (bigDiv?.parentElement) {
              bigDiv.parentElement.style.height = `${bigDivHeight}px`;
              containerDiv1?.setAttribute('style', 'overflow: hidden');
              containerDiv1.style.width = `${containerDiv1Width}px`;
              containerDiv2.style.height = `${containerDiv1Height}px`;
              containerDiv3.style.height = `${containerDiv1Height}px`;
              containerDiv4.style.height = `${containerDiv1Height}px`;
              containerDiv4.style.overflow = 'hidden';
              containerDiv5.style.height = `${containerDiv5Height}px`;
              containerDiv5.style.width = `${containerDiv5Width}px`;
              containerDiv5.style.overflow = 'auto';
            }

            if (
              yesDoms[index]?.children[1].getAttribute('data-test-viz-type') ===
              'pivot_table'
            ) {
              // 重置第二种
              tableDiv.style.width = `${tableDivWidth}px`;
              tableDiv.style.maxWidth = `${tableDivWidth}px`;
              tableDiv.style.height = `${tableDivHeight}px`;
              tableDiv.style.maxHeight = `${tableDivHeight}px`;
              tableDiv1.style.overflow = 'hidden';
              tableDiv2.style.overflow = 'hidden';
              tableDiv3.style.maxHeight = `${tableDiv3Height}px`;
              tableDiv3.style.overflow = 'auto';
              tableDiv4.style.overflow = 'hidden';
            }

            index += 1;
            if (index >= idLength) {
              resolve(position);
            } else {
              next(lastPosition);
            }
          })
          .catch(err => {
            reject(err);
          });
      }
      next(0);
    }).then(() => {
      const filename = `${generateFileStem(description)}.pdf`;

      pdf.save(filename);

      // delete the container
      document.querySelector('.pdf-flag-doms')?.remove();
    });
  };
}
