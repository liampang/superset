/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import { t } from '@superset-ui/core';
import domToImage from 'dom-to-image-more';
import kebabCase from 'lodash/kebabCase';
import { SyntheticEvent } from 'react';
import { addWarningToast } from 'src/components/MessageToasts/actions';
import { jsPDF } from 'jspdf';
import _ from 'lodash';

/**
 * @remark
 * same as https://github.com/apache/superset/blob/c53bc4ddf9808a8bb6916bbe3cb31935d33a2420/superset-frontend/src/assets/stylesheets/less/variables.less#L34
 */
const GRAY_BACKGROUND_COLOR = '#F5F5F5';

/**
 * generate a consistent file stem from a description and date
 *
 * @param description title or description of content of file
 * @param date date when file was generated
 */
const generateFileStem = (description: string, date = new Date()) =>
  `${kebabCase(description)}-${date.toISOString().replace(/[: ]/g, '-')}`;

/**
 * Create an event handler for turning an element into an image
 *
 * @param selector css selector of the parent element which should be turned into image
 * @param description name or a short description of what is being printed.
 *   Value will be normalized, and a date as well as a file extension will be added.
 * @param isExactSelector if false, searches for the closest ancestor that matches selector.
 * @returns event handler
 */
export default function downloadAsPdf(
  selector: string,
  description: string,
  isExactSelector = false,
) {
  return (event: SyntheticEvent) => {
    const elementToPrint = isExactSelector
      ? (document.querySelector(selector) as HTMLElement)
      : (event.currentTarget.closest(selector) as HTMLElement);

    if (!elementToPrint) {
      return addWarningToast(
        t('PDF download failed, please refresh and try again.'),
      );
    }

    // Mapbox controls are loaded from different origin, causing CORS error
    // See https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL#exceptions
    const filter = (node: Element) => {
      if (typeof node.className === 'string') {
        return (
          node.className !== 'mapboxgl-control-container' &&
          !node.className.includes('ant-dropdown')
        );
      }
      return true;
    };
    const checkbox: HTMLElement | null = elementToPrint.querySelector(
      '[name=chart-check-box]',
    );
    const antdCheckbox: HTMLElement | null = _.get(checkbox, [
      'parentElement',
      'parentElement',
    ]);

    if (checkbox) {
      antdCheckbox!.style.display = 'none';
    }
    return domToImage
      .toJpeg(elementToPrint, {
        bgcolor: GRAY_BACKGROUND_COLOR,
        filter,
      })
      .then(dataUrl => {
        let orientation: 'p' | 'portrait' | 'l' | 'landscape' = 'p';
        const w = elementToPrint.offsetWidth;
        const h = elementToPrint.offsetHeight;
        if (w >= h) {
          orientation = 'l';
        }
        const doc = new jsPDF({
          orientation,
          format: [w, h], // width,height
          unit: 'px',
          hotfixes: ['px_scaling'],
        });
        doc.addImage(dataUrl, 'JPEG', 0, 0, w, h);
        const filename = `${generateFileStem(description)}.pdf`;
        doc.save(filename);
      })
      .catch(e => {
        console.error('Creating pdf failed', e);
      })
      .finally(() => {
        antdCheckbox!.style.display = 'inline';
      });
  };
}
