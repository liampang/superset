/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import React from 'react';
import { t } from '@superset-ui/core';
import {
  ControlPanelConfig,
  D3_FORMAT_DOCS,
  D3_FORMAT_OPTIONS,
} from '@superset-ui/chart-controls';
import { legendSection } from '../controls';

enum MarkLineLabelPositionEnum {
  start = 'start',
  middle = 'middle',
  end = 'end',
  insideStartTop = 'insideStartTop',
  insideStartBottom = 'insideStartBottom',
  insideMiddleTop = 'insideMiddleTop',
  insideMiddleBottom = 'insideMiddleBottom',
  insideEndTop = 'insideEndTop',
  insideEndBottom = 'insideEndBottom',
}

const LABEL_POSITION: [MarkLineLabelPositionEnum, string][] = [
  [MarkLineLabelPositionEnum.start, 'start'],
  [MarkLineLabelPositionEnum.middle, 'middle'],
  [MarkLineLabelPositionEnum.end, 'end'],
  [MarkLineLabelPositionEnum.insideEndBottom, 'insideEndBottom'],
  [MarkLineLabelPositionEnum.insideEndTop, 'insideEndTop'],
  [MarkLineLabelPositionEnum.insideMiddleBottom, 'insideMiddleBottom'],
  [MarkLineLabelPositionEnum.insideMiddleTop, 'insideMiddleTop'],
  [MarkLineLabelPositionEnum.insideStartBottom, 'insideStartBottom'],
  [MarkLineLabelPositionEnum.insideStartTop, 'insideStartTop'],
];

const config: ControlPanelConfig = {
  controlPanelSections: [
    {
      label: t('Query'),
      expanded: true,
      controlSetRows: [['groupby']],
    },
    {
      label: t('Chart Options'),
      expanded: true,
      controlSetRows: [
        ['color_scheme'], // Color Scheme
        ...legendSection, // SHOW LEGEND,TYPE,ORIENTATION,MARGIN
        [<div className="section-header">{t('Labels')}</div>],
        [
          {
            name: 'show_labels', // SHOW LABELS
            config: {
              type: 'CheckboxControl',
              label: t('Show Labels'),
              renderTrigger: true,
              default: true,
              description: t('Whether to display the labels.'),
            },
          },
        ],
        [
          {
            name: 'label_type', // LABEL TYPE
            config: {
              type: 'SelectControl',
              label: t('Label Type'),
              default: 'value',
              renderTrigger: true,
              choices: [
                ['value', 'Value'],
                ['key_value', 'Category and Value'],
              ],
              description: t('What should be shown on the label?'),
            },
          },
        ],
        [
          {
            name: 'label_position', // LABEL POSITION
            config: {
              type: 'SelectControl',
              freeForm: false,
              label: t('Label position'),
              renderTrigger: true,
              choices: LABEL_POSITION,
              default: MarkLineLabelPositionEnum.end,
              description: D3_FORMAT_DOCS,
            },
          },
        ],
        [
          {
            name: 'number_format', // NUMBER FORMAT
            config: {
              type: 'SelectControl',
              freeForm: true,
              label: t('Number format'),
              renderTrigger: true,
              default: 'SMART_NUMBER',
              choices: D3_FORMAT_OPTIONS,
              description: D3_FORMAT_DOCS,
            },
          },
        ],
      ],
    },
  ],
};

export default config;
