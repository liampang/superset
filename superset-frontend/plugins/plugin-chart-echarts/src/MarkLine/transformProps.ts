/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import {
  NumberFormatter,
  CategoricalColorNamespace,
  getNumberFormatter,
} from '@superset-ui/core';
import { CallbackDataParams } from 'echarts/types/src/util/types';
import { EChartsCoreOption } from 'echarts';
import {
  EchartsMarkLineChartProps,
  EchartsRadarLabelType,
  MarkLineChartTransformedProps,
} from './types';
import { getChartPadding } from '../utils/series';

export function formatLabel({
  params,
  labelType,
  numberFormatter,
}: {
  params: CallbackDataParams;
  labelType: EchartsRadarLabelType;
  numberFormatter: NumberFormatter;
}): string {
  const { name = '', value } = params;
  const formattedValue = numberFormatter(value as number);

  switch (labelType) {
    case EchartsRadarLabelType.Value:
      return formattedValue;
    case EchartsRadarLabelType.KeyValue:
      return `${name}: ${formattedValue}`;
    default:
      return name;
  }
}

export default function transformProps(
  chartProps: EchartsMarkLineChartProps,
): MarkLineChartTransformedProps {
  const { width, height, formData, queriesData } = chartProps;
  const { data: records } = queriesData[0] || {};
  const {
    showLabels,
    colorScheme,
    numberFormat,
    labelType,
    legendMargin,
    legendOrientation,
    // legendType,
    showLegend,
    sliceId,
    labelPosition,
  } = formData;

  const colorFn = CategoricalColorNamespace.getScale(colorScheme as string);
  const numberFormatter = getNumberFormatter(numberFormat);
  const chartPadding = getChartPadding(
    showLegend,
    legendOrientation,
    legendMargin,
    { top: 30, left: 60, right: 60, bottom: 40 },
  );
  const selectedValues: Record<number, string> = {};
  const markLine = [];
  const countries = records.map(i => i.country);

  const irScores = records.map(i => i.irScore);

  let worldMax = 0;
  let worldMedian = 0;

  worldMax = records.reduce(
    (previousValue, currentValue) =>
      Math.max(previousValue, currentValue.worldMax as number),
    records[0].worldMax as number,
  );

  worldMedian = records.reduce(
    (previousValue, currentValue) =>
      Math.max(previousValue, currentValue.worldMedian as number),
    records[0].worldMedian as number,
  );

  for (let i = 0; i < countries.length; i += 1) {
    markLine.push({
      yAxis: i,
      label: {
        show: showLabels,
        formatter: '{b}',
        position: countries[i],
      },
      itemStyle: {
        color: colorFn(countries[i] as string, sliceId),
      },
    });
  }

  markLine.push({
    name: 'World Median IR',
    xAxis: worldMedian,
    label: {
      show: showLabels,
      formatter: (params: any) =>
        formatLabel({ params, numberFormatter, labelType }),
      position: labelPosition,
    },
  });

  markLine.push({
    name: 'World Max IR',
    xAxis: worldMax,
    label: {
      show: showLabels,
      formatter: (params: any) =>
        formatLabel({ params, numberFormatter, labelType }),
      position: labelPosition,
    },
  });

  const echartOptions: EChartsCoreOption = {
    animation: false,
    textStyle: {
      fontSize: 14,
    },
    xAxis: {
      type: 'value',
      max: worldMax,
      boundaryGap: true,
      splitArea: {
        show: true,
      },
    },
    yAxis: {
      type: 'category',
      data: countries,
    },
    series: [
      {
        name: 'line',
        type: 'scatter',
        stack: 'all',
        symbolSize: 10,
        data: irScores,
        label: {
          show: false,
          position: 'top',
        },
        markLine: {
          data: markLine,
        },
      },
    ],
    grid: {
      ...chartPadding,
    },
  };

  return {
    formData,
    width,
    height,
    echartOptions,
    selectedValues,
  };
}
